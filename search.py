import sys
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from Airport import Airport


engine = create_engine('mysql://root:pass@127.0.0.1:3306/mydb', echo=True)


def search_by_code(code, airport, session):
    for airport in session.query(airport).filter_by(ident=code):
        d = dict([('ident', airport.ident), ('tipe', airport.tipe), ('name', airport.name),
                  ('elevation_ft', airport.elevation_ft), ('continent', airport.continent),
                  ('iso_country', airport.iso_country), ('iso_region', airport.iso_region),
                  ('municipality', airport.municipality), ('gps_code', airport.gps_code),
                  ('iata_code', airport.iata_code), ('local_code', airport.local_code),
                  ('latitude', airport.latitude), ('longitude', airport.longitude)])
        return d


if __name__ == "__main__":
    CODE = sys.argv[1]
    Session_search = sessionmaker(bind=engine)
    s = Session_search()
    info = search_by_code(CODE, Airport, s)
    print(info['name'])