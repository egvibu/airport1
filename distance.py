import sys
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from Airport import Airport
from search import search_by_code
from vincenty import vincenty


engine = create_engine('mysql://root:pass@127.0.0.1:3306/mydb', echo=True)


def distance(code1, code2, airport, session):
    d1 = search_by_code(code1, airport, session)
    d2 = search_by_code(code2, airport, session)
    S = vincenty([float(d1['latitude']), float(d1['longitude'])],
                 [float(d2['latitude']), float(d2['longitude'])])

    print(d1['latitude'], d1['longitude'])
    print(d2['latitude'], d2['longitude'])
    return S


if __name__ == "__main__":
    CODE1 = sys.argv[1]
    CODE2 = sys.argv[2]
    Session_search = sessionmaker(bind=engine)
    s = Session_search()
    info = distance(CODE1, CODE2, Airport, s)
    print("Distance: ", info)